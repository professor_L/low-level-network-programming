CC = gcc
CFLAGS = -g -Wall -lpcap -lnet

SOURCES = pcap_net.c std_utility.c net_utility.c dhcp_option.c dhcp.c dns.c

exec: ${SOURCES} main.c
	${CC} -o exec main.c ${SOURCES} ${CFLAGS}

test: main_test.c ${SOURCES}
	${CC} -o test main_test.c ${SOURCES} ${CFLAGS}
