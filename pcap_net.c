#include "pcap_net.h"
#include "pcap_net_defines.h"
#include "net_utility.h"
#include "std_utility.h"

int netcap_init_pcap(pcap_t **handle, const char *dev)
{
	char errbuf[PCAP_ERRBUF_SIZE];
	*handle = pcap_open_live(dev, BUFSIZ, 1, NETCAP_SNAPLEN, errbuf);

	if ( *handle == NULL )
	{
		printf("pcap_open_live: %s.\n", errbuf);
		return -1;
	}

	/*
	if ( pcap_set_promisc(*handle, 1) != 0)
	{
		printf("pcap_set_promisc error.\n");
		return -1;
	}

	if ( pcap_set_rfmon(*handle, 0) != 0 )
	{
		printf("pcap_set_rfmon error.\n");
		return -1;
	}

	if ( pcap_set_snaplen(*handle, NETCAP_SNAPLEN) != 0 )
	{
		printf("pcap_set_snaplen error.\n");
		return -1;
	}
	//two more setting: buffer timeout and immediate mode
	

	if ( pcap_activate(*handle) < 0 )
	{
		printf("pcap_activate.\n");
		return -1;
	}
	*/

	return 0;
}

int netcap_prepare_filter(pcap_t **handle, const char *dev, const char *filter_exp)
{
	struct bpf_program fp;

	if ( pcap_compile(*handle, &fp, filter_exp, 0, 0xFFFFFF00) == -1 )
	{
		printf("Can't parse filter %s: %s\n", filter_exp, pcap_geterr(*handle));
		return -1;
	}

	if ( pcap_setfilter(*handle, &fp) == -1 )
	{
		printf("Could not install filter %s: %s\n", filter_exp, pcap_geterr(*handle));
		return -1;
	}


	return 0;
}

void netcap_print_packet(const uint8_t *packet)
{
	struct libnet_ethernet_hdr *ether;
	union network_hdr net_hdr;
	union transport_hdr trans_hdr;
	union application_hdr app_hdr;

	//parse packet
	netcap_parse_packet(packet, &ether, &net_hdr, &trans_hdr, &app_hdr);

	//print ethernet
	netcap_print_ether_hdr(ether);

	//print ip
	int ip_ver = netcap_get_ip_version(&net_hdr);
	if ( ip_ver == 4 )
		netcap_print_ipv4_hdr(net_hdr.ipv4_hdr);
	else if ( ip_ver == 6 )
		netcap_print_ipv6_hdr(net_hdr.ipv6_hdr);
	else
	{
		printf("Not an IP packet.\n");
		return;
	}

	//print tcp/udp
	uint8_t ip_proto = netcap_get_ip_proto(&net_hdr);
	if ( ip_proto == IP_PROTO_TCP )
		netcap_print_tcp_hdr(trans_hdr.tcp_hdr);
	else if ( ip_proto == IP_PROTO_UDP )
		netcap_print_udp_hdr(trans_hdr.udp_hdr);
	else
	{
		printf("Not recognized transport service.\n");
		return;
	}

	//print application
	uint16_t portnum = netcap_get_portnum( (uint8_t*)trans_hdr.tcp_hdr);
	printf("portnum = %u\n", portnum);
	if ( portnum == 67 || portnum == 68)
		netcap_dhcpv4_print_hdr(app_hdr.dhcpv4_hdr);
	else if ( portnum == 53 )
		netcap_dns_print( (uint8_t*)app_hdr.dnsv4_hdr, stdout);

}

void netcap_print_ether_hdr(const struct libnet_ethernet_hdr *hdr)
{
	printf("\n---------------- ETHERNET HEADER -----------------\n");
	printf("Src address: %2X:%2X:%2X:%2X:%2X:%2X \n",
					hdr->ether_shost[0], hdr->ether_shost[1], hdr->ether_shost[2],
					hdr->ether_shost[3], hdr->ether_shost[4], hdr->ether_shost[5]);

	printf("Des address: %2X:%2X:%2X:%2X:%2X:%2X \n",
					hdr->ether_dhost[0], hdr->ether_dhost[1], hdr->ether_dhost[2],
					hdr->ether_dhost[3], hdr->ether_dhost[4], hdr->ether_dhost[5]);

	netcap_print_ether_type( ntohs(hdr->ether_type) );
}

inline
void netcap_print_ether_type(const uint16_t type)
{
	switch (type)
	{
		case ETHERTYPE_IP:
			printf("IPv4 packet.\n");
			break;
		case ETHERTYPE_IPV6:
			printf("IPv6 packet.\n");
			break;
		case ETHERTYPE_ARP:
			printf("ARP packet.\n");
			break;
		case ETHERTYPE_EAP:
			printf("EAP packet.\n");
			break;
		default:
			printf("Unknown type.\n");
	}
}

void netcap_print_ipv4_hdr(const struct libnet_ipv4_hdr *hdr)
{
	printf("\n---------------- IPV4 HEADER -----------------\n");
	uint8_t *byte = (uint8_t *)&(hdr->ip_src);
	printf("IP src: %hhu.%hhu.%hhu.%hhu\n", byte[0], byte[1], byte[2], byte[3]);

	byte = (uint8_t *)&(hdr->ip_dst);
	printf("IP dst: %hhu.%hhu.%hhu.%hhu\n", byte[0], byte[1], byte[2], byte[3]);

	printf("IP len: %hu\n", ntohs(hdr->ip_len));	//header + data len
	printf("IP ID : %hu\n", ntohs(hdr->ip_id));
	printf("IP TTL: %hu\n", hdr->ip_ttl);
	printf("IP SUM: %hu\n", ntohs(hdr->ip_sum));
	netcap_print_ipv4_proto(hdr->ip_p);
}

inline
void netcap_print_ipv4_proto(const uint8_t proto)
{
	switch (proto)
	{
		case IP_PROTO_TCP:	//tcp
			printf("IP proto: TCP\n");
			break;

		case IP_PROTO_UDP: //udp
			printf("IP proto: UDP\n");
			break;

		case IP_PROTO_ICMP: //icmp
			printf("IP proto: ICMP\n");
			break;
	}
}

void netcap_print_tcp_hdr(const struct libnet_tcp_hdr *hdr)
{
	printf("\n------------ TCP header -------------\n");
	printf("src port: %u\n", ntohs(hdr->th_sport));
	printf("dst port: %u\n", ntohs(hdr->th_dport));

	printf("seq num : %u\n", ntohl(hdr->th_seq));
	printf("ack num : %u\n", ntohl(hdr->th_ack));

	printf(" flags  : %2X\n", hdr->th_flags);
}

void netcap_print_udp_hdr(const struct libnet_udp_hdr *hdr)
{
	printf("\n------------ UDP header -------------\n");
	printf("src port: %u\n", ntohs(hdr->uh_sport));
	printf("dst port: %u\n", ntohs(hdr->uh_dport));
	printf("length  : %u\n", ntohs(hdr->uh_ulen));
}



inline
uint16_t netcap_get_portnum(const uint8_t *trans_seg)
{
	uint16_t sport, dport;
	memcpy(&sport, trans_seg, 2);
	memcpy(&dport, trans_seg+2, 2);
	sport = ntohs(sport);
	dport = ntohs(dport);

	if ( sport < dport )
		return sport;
	else
		return dport;
}

void netcap_print_ipv6_hdr(const struct libnet_ipv6_hdr *hdr)
{
	printf("\n--------------- IPv6 header -------------------\n");
	printf("payload length: %u\n", ntohs(hdr->ip_len));
	printf("next header:    %hu\n", hdr->ip_nh);
	printf("hop limit:	%hu\n", hdr->ip_hl);
	printf("src addr:  ");
	netcap_print_ipv6_addr( &(hdr->ip_src) );
	printf("dst addr:  ");
	netcap_print_ipv6_addr( &(hdr->ip_dst) );
}

void netcap_print_ipv6_addr(const struct libnet_in6_addr *addr)
{
	printf("%02X%02X:%02X%02X:%02X%02X:%02X%02X:%02X%02X:%02X%02X:%02X%02X:%02X%02X \n",
				addr->__u6_addr.__u6_addr8[0], addr->__u6_addr.__u6_addr8[1],
				addr->__u6_addr.__u6_addr8[2], addr->__u6_addr.__u6_addr8[3],
				addr->__u6_addr.__u6_addr8[4], addr->__u6_addr.__u6_addr8[5],
				addr->__u6_addr.__u6_addr8[6], addr->__u6_addr.__u6_addr8[7],
				addr->__u6_addr.__u6_addr8[8], addr->__u6_addr.__u6_addr8[9],
				addr->__u6_addr.__u6_addr8[10], addr->__u6_addr.__u6_addr8[11],
				addr->__u6_addr.__u6_addr8[12], addr->__u6_addr.__u6_addr8[13],
				addr->__u6_addr.__u6_addr8[14], addr->__u6_addr.__u6_addr8[15]);
}

int netcap_dhcp_drain_pool(pcap_t *pcap_h, libnet_t *libnet_h)
{
	//get MAC address
	uint8_t mac_addr[6];
	mac_addr[0] = 0x28;
	mac_addr[1] = 0x3F;
	mac_addr[2] = 0x69;
	mac_addr[3] = 0x55;
	mac_addr[4] = 0x92;
	mac_addr[5] = 0xD7;
	//std_util_random_bytes(mac_addr+5, 1);

	uint32_t transid = 0x01020304;

	//create DHCP packet
	libnet_ptag_t dhcp_tag = 0;
	dhcp_tag = netcap_dhcpv4_create_request_discovery(
					&transid, mac_addr, libnet_h, &dhcp_tag);

	//send crafted packet
	libnet_write(libnet_h);
	printf("send a packet.\n");

	//receive reply from server
	struct pcap_pkthdr header;
	const uint8_t *srv_pkt = pcap_next(pcap_h, &header);
	printf("receive a packet.\n");
	netcap_print_packet(srv_pkt);

	
	//get YIP
	uint32_t yip = netcap_dhcpv4_get_yip(srv_pkt);

	//create another packet
	dhcp_tag = 0;
	dhcp_tag = netcap_dhcpv4_create_request_request(
							&transid, mac_addr, &yip, libnet_h, &dhcp_tag);

	//send crafted packet
	libnet_write(libnet_h);
	printf("send a packet.\n");

	//receive another reply from server
	srv_pkt = pcap_next(pcap_h, &header);
	printf("receive a packet.\n");
	netcap_print_packet(srv_pkt);

	return 0;
}

int netcap_parse_packet(const uint8_t* packet, struct libnet_ethernet_hdr **ether,
							union network_hdr *net_hdr, union transport_hdr *trans_hdr,
							union application_hdr *app_hdr)
{
	if ( packet == NULL )
	{
		*ether = NULL;
		memset(net_hdr, 0, sizeof(*net_hdr));
		memset(trans_hdr, 0, sizeof(*trans_hdr));
		memset(app_hdr, 0, sizeof(*app_hdr));
		return -1;
	}

	const uint8_t *pkt_offset = packet;

	//handle ether
	*ether = (struct libnet_ethernet_hdr *)pkt_offset;
	pkt_offset += LIBNET_ETH_H;

	//handle IP
	net_hdr->ipv4_hdr = (struct libnet_ipv4_hdr*)pkt_offset;
	uint16_t upper_proto = 0;
	uint16_t ether_type = ntohs( (*ether)->ether_type );
	if ( ether_type == ETHERTYPE_IP )
	{
		upper_proto = net_hdr->ipv4_hdr->ip_p;
		pkt_offset += LIBNET_IPV4_H;
	}
	else if (ether_type == ETHERTYPE_IPV6)
	{
		upper_proto = net_hdr->ipv6_hdr->ip_nh;
		pkt_offset += LIBNET_IPV6_H;
	}

	//handle transport
	trans_hdr->tcp_hdr = (struct libnet_tcp_hdr*)pkt_offset;
	if ( upper_proto == IP_PROTO_TCP )
		pkt_offset += LIBNET_TCP_H;
	else if (upper_proto == IP_PROTO_UDP )
		pkt_offset += LIBNET_UDP_H;

	//handle application
	app_hdr->dhcpv4_hdr = (struct libnet_dhcpv4_hdr *)pkt_offset;
	return 0;
}

inline int netcap_get_ip_version(const union network_hdr *net_hdr)
{
	return net_hdr->ipv4_hdr->ip_v;
}

uint8_t netcap_get_ip_proto(const union network_hdr *net_hdr)
{
	uint8_t ip_ver = net_hdr->ipv4_hdr->ip_v;
	if ( ip_ver == 4 )
		return net_hdr->ipv4_hdr->ip_p;
	else if ( ip_ver == 6 )
		return net_hdr->ipv6_hdr->ip_nh;
	else
		return -1;
}

void netcap_exchange_dns_query(pcap_t *handle, libnet_t *libnet_h, char *name)
{
	//create and send DNS packet
	netcap_dns_create_query(name, libnet_h);
	libnet_write(libnet_h);

	//receive and print
	struct pcap_pkthdr hd;
	pcap_next(handle, &hd);
	const uint8_t *packet = pcap_next(handle, &hd);
	netcap_print_packet(packet);
}
