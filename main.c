#include <stdio.h>

#include "pcap_net.h"
#include <time.h>

int main(int argc, char *argv[])
{
	printf("Hello world.\n");
	srand(time(NULL));

	if ( argc != 3 )
		return printf("Argument must be 3.\n");

	char *dev = "wlp3s0";
	char *filter_exp = argv[1];
	char *name = argv[2];
	pcap_t *pcap_handle;

	int ret = netcap_init_pcap(&pcap_handle, dev);
	if ( ret < 0 )
		return printf("Init pcap fails.\n");

	printf("Init pcap successfully.\n");

	ret = netcap_prepare_filter(&pcap_handle, dev, filter_exp);
	if ( ret < 0 )
		return printf("Set filter fails.\n");

	printf("Set filters successfully.\n");

	libnet_t *l;
	char errbuf[LIBNET_ERRBUF_SIZE];
	l = libnet_init(LIBNET_RAW4, NULL, errbuf);
	if ( l == NULL )
	{
		printf("libnet init fails.\n");
		return -1;
	}

	netcap_exchange_dns_query(pcap_handle, l, name);

	pcap_close(pcap_handle);
	libnet_destroy(l);

	return 0;
}
