#ifndef		__TRANSPORT_LAYER__
#define		__TRANSPORT_LAYER__

#include <libnet.h>

union transport_hdr {
	struct libnet_udp_hdr *udp_hdr;
	struct libnet_tcp_hdr *tcp_hdr;
};

#endif
