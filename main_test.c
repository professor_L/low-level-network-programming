#include "std_utility.h"

#include "dns.h"

int main(int argc, char *argv[])
{
	char *name = argv[1];
	char *decode;
	uint8_t *encode;
	size_t dsize, esize;

	netcap_dns_name_encode(name, &encode, &esize);
	netcap_dns_name_decode(encode, &decode);

	printf("%u %u\n", esize, netcap_dns_notation_len(encode));

	std_util_hexdump(name, strlen(name)+1, "name");
	std_util_hexdump(encode, esize, "encode");
	std_util_hexdump(decode, strlen(decode)+1, "decode");

	free(encode);
	free(decode);
	return 0;
}
