#ifndef		__DHCP_OPTIONS__
#define		__DHCP_OPTIONS__

#include <stdint.h>
#include <string.h>

struct netcap_dhcp_option {
	uint8_t type;
	uint8_t len;
	uint8_t *value;
	struct netcap_dhcp_option *next;
};

struct netcap_dhcp_option* netcap_dhcp_option_new(void);
struct netcap_dhcp_option* netcap_dhcp_option_create(uint8_t type, uint8_t len, uint8_t *value);
void netcap_dhcp_option_free(struct netcap_dhcp_option **node);
void netcap_dhcp_option_prepend(struct netcap_dhcp_option **head, struct netcap_dhcp_option *node);
void netcap_dhcp_option_delete(struct netcap_dhcp_option **node);
void netcap_dhcp_option_print(const struct netcap_dhcp_option *op);
struct netcap_dhcp_option* netcap_dhcp_option_build(void);
int netcap_dhcp_option_set_value(uint8_t type, uint8_t *len, uint8_t **value);
int netcap_dhcp_option_set_msg_type(uint8_t *len, uint8_t **value);
int netcap_dhcp_option_set_hostname(uint8_t *len, uint8_t **value);
int netcap_dhcp_option_set_requestIP(uint8_t *len, uint8_t **value);
int netcap_dhcp_option_set_serverID(uint8_t *len, uint8_t **value);
size_t netcap_dhcp_option_total_len(struct netcap_dhcp_option *head);
int netcap_dhcp_option_payload(struct netcap_dhcp_option *head, uint8_t **payload, size_t *pay_len);
int netcap_dhcp_option_transfer(struct netcap_dhcp_option *head, uint8_t **payload);

#endif
