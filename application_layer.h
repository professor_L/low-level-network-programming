#ifndef		__APPLICATION_LAYER__
#define		__APPLICATION_LAYER__

#include <libnet.h>

union application_hdr {
	struct libnet_dhcpv4_hdr *dhcpv4_hdr;
	struct libnet_dnsv4udp_hdr *dnsv4_hdr;
};

#endif
