#ifndef		__PCAP_NET__
#define		__PCAP_NET__

#include <pcap.h>
#include <libnet.h>

#include "transport_layer.h"
#include "application_layer.h"
#include "network_layer.h"

#include "dhcp.h"
#include "dns.h"

#define NETCAP_SNAPLEN	1000

int netcap_init_pcap(pcap_t **handle, const char *dev);
int netcap_prepare_filter(pcap_t **handle, const char *dev, const char *filter_exp);
int netcap_parse_packet(const uint8_t* packet, struct libnet_ethernet_hdr **ether,
							union network_hdr *net_hdr, union transport_hdr *trans_hdr,
							union application_hdr *app_hdr);

void netcap_got_packet(u_char *args, const struct pcap_pkthdr *header, const u_char *packet);

void netcap_print_packet(const uint8_t *pkt);
void netcap_print_ether_hdr(const struct libnet_ethernet_hdr *ether);
void netcap_print_ether_type(const uint16_t type);
void netcap_print_ipv4_hdr(const struct libnet_ipv4_hdr *ip);
void netcap_print_ipv4_proto(const uint8_t proto);
void netcap_print_tcp_hdr(const struct libnet_tcp_hdr *tcp);
void netcap_print_udp_hdr(const struct libnet_udp_hdr *udp);

void netcap_print_ipv6_hdr(const struct libnet_ipv6_hdr *ipv6);
void netcap_print_ipv6_addr(const struct libnet_in6_addr *addr);

uint16_t netcap_get_portnum(const uint8_t *trans_seg);

int netcap_dhcp_drain_pool(pcap_t *pcap_h, libnet_t *libnet_h);

int netcap_get_ip_version(const union network_hdr *net_hdr);

uint8_t netcap_get_ip_proto(const union network_hdr *net_hdr);

void netcap_exchange_dns_query(pcap_t *handle, libnet_t *libnet_h, char *name);

#endif
