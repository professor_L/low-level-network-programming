#include "dhcp_option.h"
#include "std_utility.h"

#include <libnet.h>

struct netcap_dhcp_option* netcap_dhcp_option_new(void)
{
	struct netcap_dhcp_option * ret;
	ret = (struct netcap_dhcp_option*)malloc( sizeof(*ret) );
	if ( ret != NULL )
		memset(ret, 0, sizeof(*ret));
	return ret;
}


struct netcap_dhcp_option* netcap_dhcp_option_create(uint8_t type, uint8_t len, uint8_t *value)
{
	struct netcap_dhcp_option * ret;
	ret = (struct netcap_dhcp_option*)malloc( sizeof(*ret) );
	if ( ret != NULL )
	{
		ret->type = type;
		ret->len = len;
		ret->next = NULL;
		if ( value != NULL )
			ret->value = value;
		else
			ret->value = NULL;
	}
	return ret;
}


void netcap_dhcp_option_free(struct netcap_dhcp_option **node)
{
	if ( *node != NULL )
	{
		if ( (*node)->value != NULL )
			free( (*node)->value);

		memset(*node, 0, 2 + (*node)->len);
		free(*node);
		*node = NULL;
	}
}


void netcap_dhcp_option_delete(struct netcap_dhcp_option **node)
{
	if ( *node != NULL )
	{
		do
		{
			struct netcap_dhcp_option *tmp = (*node)->next;
			netcap_dhcp_option_free(node);
			*node = tmp;
		}while(*node);
	}
}


void netcap_dhcp_option_print(const struct netcap_dhcp_option *op)
{
	while(op)
	{
		printf("type: %02X\n",op->type);
		printf("len : %02X\n",op->len);
		std_util_hexdump(op->value, op->len, "value");
		op = op->next;
	}
}


void netcap_dhcp_option_prepend(struct netcap_dhcp_option **head, struct netcap_dhcp_option *node)
{
	if ( node == NULL )
		return;

	if ( *head == NULL )
	{
		*head = node;
		return;
	}

	node->next = *head;
	*head = node;
}


int netcap_dhcp_option_set_value(uint8_t type, uint8_t *len, uint8_t **value)
{
	printf("input value: ");
	switch (type)
	{
		case LIBNET_DHCP_SUBNETMASK:
			*len = 4;
			*value = (uint8_t*)malloc( *len );
			if ( *value == NULL )
				return -1;
			scanf("%hhu.%hhu.%hhu.%hhu", &(*value)[0], &(*value)[1], &(*value)[2], &(*value)[3]);
			break;

		default:
			*len = 1;
			*value = (uint8_t*)malloc( *len );
			if ( *value == NULL )
				return -1;
			scanf("%hhu", &(*value)[0]);
			break;
	}
	return 0;
}


struct netcap_dhcp_option* netcap_dhcp_option_build(void)
{
	struct netcap_dhcp_option *options = NULL , *node;

	uint8_t type;
	uint8_t len;
	uint8_t *value;

	int yes = 0;
	do
	{
		printf("type: ");
		scanf("%hhu", &type);
		netcap_dhcp_option_set_value(type, &len, &value);
		node = netcap_dhcp_option_create(type, len, value);
		netcap_dhcp_option_prepend(&options, node);

		printf("More options ? ");
		scanf("%d",&yes);
	}while(yes);
	return options;
}


//this function needs rework
int netcap_dhcp_option_set_msg_type(uint8_t *len, uint8_t **value)
{
	*len = 1;
	*value = (uint8_t*)malloc( *len );
	if ( *value == NULL )
		return -1;
	scanf("%hhu", *value);
	return 0;
}


int netcap_dhcp_option_set_hostname(uint8_t *len, uint8_t **value)
{
	char buff[STD_UTIL_MAX_STR_LEN];
	std_util_fgets(buff, STD_UTIL_MAX_STR_LEN, stdin);
	*len = strlen(buff);
	*value = (uint8_t*)malloc( *len );
	if ( *value == NULL )
		return -1;
	memcpy(*value, buff, *len);
	return 0;
}


int netcap_dhcp_option_set_requestIP(uint8_t *len, uint8_t **value)
{
	*len = 4;
	*value = (uint8_t*)malloc( *len );
	if ( *value == NULL )
		return -1;
	scanf("%hhu.%hhu.%hhu.%hhu", &(*value)[0], &(*value)[1], &(*value)[2], &(*value)[3]);
	return 0;
}


int netcap_dhcp_option_set_serverID(uint8_t *len, uint8_t **value)
{
	*len = 4;
	*value = (uint8_t*)malloc( *len );
	if ( *value == NULL )
		return -1;
	scanf("%hhu.%hhu.%hhu.%hhu", &(*value)[0], &(*value)[1], &(*value)[2], &(*value)[3]);
	return 0;
}


size_t netcap_dhcp_option_total_len(struct netcap_dhcp_option *head)
{
	size_t ret = 0;
	struct netcap_dhcp_option *tmp = head;
	while (tmp)
	{
		ret += tmp->len + 2;
		tmp = tmp->next;
	}
	return ret;
}


int netcap_dhcp_option_payload(struct netcap_dhcp_option *head, uint8_t **payload, size_t *pay_len)
{
	if ( head == NULL )
	{
		*pay_len = 0;
		*payload = NULL;
		return -1;
	}

	*pay_len = netcap_dhcp_option_total_len(head);
	*payload = (uint8_t*)malloc( *pay_len );
	if ( *payload != NULL )
	{
		netcap_dhcp_option_transfer(head, payload);
		return 0;
	}
	else
	{
		*pay_len = 0;
		*payload = NULL;
		return -1;
	}
}


int netcap_dhcp_option_transfer(struct netcap_dhcp_option *head, uint8_t **payload)
{
	uint8_t *data = *payload;
	struct netcap_dhcp_option *tmp = head;
	size_t offset = 0;
	while (tmp)
	{
		data[offset] = tmp->type;
		data[offset+1] = tmp->len;
		memcpy(data + offset + 2, tmp->value, tmp->len);

		//update offset
		offset += 2 + tmp->len;

		tmp = tmp->next;
	}
	return 0;
}
