#ifndef		__NETWORK__LAYER__
#define		__NETWORK__LAYER__

#include <libnet.h>

union network_hdr {
	struct libnet_ipv4_hdr *ipv4_hdr;
	struct libnet_ipv6_hdr *ipv6_hdr;
};

#endif
