#include "net_utility.h"

inline
void net_util_print_ipv4(const uint32_t addr, const char *desc)
{
	uint8_t *byte = (uint8_t*)&addr;
	printf("%s: %u.%u.%u.%u\n", desc, byte[0], byte[1], byte[2], byte[3]);
}
