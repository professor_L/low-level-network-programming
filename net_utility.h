#ifndef		__NETWORK_UTILITY__
#define		__NETWORK_UTILITY__

#include <stdint.h>
#include <stdio.h>

void net_util_print_ipv4(const uint32_t addr, const char *desc);

#endif
