#ifndef		__NETCAP_DHCP__
#define		__NETCAP_DHCP__

#include <stdio.h>
#include <stdint.h>
#include <libnet.h>

#include "pcap_net_defines.h"


void netcap_dhcpv4_print_hdr(const struct libnet_dhcpv4_hdr *dhcp);
void netcap_dhcpv4_print_opcode(const uint8_t opcode);
void netcap_dhcpv4_print_hwtype(const uint8_t type);

libnet_ptag_t netcap_dhcpv4_create_request_discovery(const uint32_t *xid, const uint8_t *mac, 
											libnet_t *l, const libnet_ptag_t *tag );

libnet_ptag_t netcap_dhcpv4_create_request_request(const uint32_t *xid, const uint8_t *mac, const uint32_t *req_ip,
											libnet_t *l, const libnet_ptag_t *tag);

uint8_t* netcap_dhcpv4_create_discovery_payload(size_t *pay_size);
uint8_t* netcap_dhcpv4_create_request_payload(size_t *pay_size, const uint32_t *yip);
uint32_t netcap_dhcpv4_get_yip(const uint8_t *pkt);
#endif
