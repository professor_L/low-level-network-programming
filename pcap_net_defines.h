#ifndef		__NETCAP_DEFINES__
#define		__NETCAP_DEFINES__

#define	GET_MSBIT(byte,n)	( (byte) >> ( 8 - (n) ) )
#define GET_LSBIT(byte,n)	( (byte) &  ( ( 1 <<  ((n)+1)) - 1   ) )

#define		IP_PROTO_TCP			0x06
#define		IP_PROTO_UDP			0x11
#define		IP_PROTO_ICMP			0x01

#define		DHCP_HWTYPE_ETHER		0x01
#define		DHCP_HWTYPE_IEEE802		0x06

#endif
