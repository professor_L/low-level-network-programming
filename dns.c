#include "dns.h"

#include <string.h>

void netcap_dns_print(const uint8_t *pkt, FILE *fp)
{
	struct libnet_dnsv4udp_hdr *dns = (struct libnet_dnsv4udp_hdr*) pkt;
	//print header
	fprintf(fp, "-------- DNS HEADER --------\n");
	fprintf(fp, "ID:        %02X\n", ntohs(dns->id));
	fprintf(fp, "FLAGS:     %02X\n", ntohs(dns->flags));
	fprintf(fp, "NUM_Q:     %02X\n", ntohs(dns->num_q));
	fprintf(fp, "NUM_ANS:   %02X\n", ntohs(dns->num_answ_rr));
	fprintf(fp, "NUM_AUTH:  %02X\n", ntohs(dns->num_auth_rr));
	fprintf(fp, "NUM_ADDI:  %02X\n", ntohs(dns->num_addi_rr));

	pkt += LIBNET_UDP_DNSV4_H;
	//print payload
	for (int i = 0; i < ntohs(dns->num_q); ++i)
	{
		printf("query_len = %lu\n", netcap_dns_query_len(pkt));
		netcap_dns_query_print(pkt);
		pkt += netcap_dns_query_len(pkt);
		printf("\n");
	}

	for (int i = 0; i < ntohs(dns->num_answ_rr); ++i)
	{
		printf("answer len = %lu\n", netcap_dns_rr_len(pkt));
		netcap_dns_rr_print(pkt);
		pkt += netcap_dns_rr_len(pkt);
		printf("\n");
	}
}

int netcap_dns_name_encode(const char *name, uint8_t **notation, size_t *len)
{
	size_t namelen = strlen(name) + 1;
	*len = namelen + 1;
	*notation = (uint8_t*)malloc( *len );

	if ( *notation != NULL)
	{
		char *ptr = (char*)(*notation);

		memcpy( ptr + 1, name, namelen );
		char *pos = strchr( ptr + 1, '.');
		ptr[0] = pos - ptr - 1;

		char *pre_pos = pos;
		while( (pos = strchr(pre_pos + 1, '.')) )
		{
			*pre_pos = pos - pre_pos - 1;
			pre_pos = pos;
		}

		*pre_pos = *len - (pre_pos - ptr + 1) - 1;
	}

	return -1;
}

int netcap_dns_name_decode(const uint8_t *notation, char **name)
{
	size_t nolen = netcap_dns_notation_len(notation);
	size_t name_len = nolen - 1;
	*name = (char*)malloc(name_len);
	if (*name)
	{
		char *ptr = *name;
		memcpy(ptr, notation + 1, name_len);
		int len = notation[0];
		int index = -1;
		while(len)
		{
			index += len + 1;
			ptr[index] = '.';
			len = notation[index + 1];
		}
		ptr[name_len - 1] = '\0';
		return 0;
	}
	return -1;
}

int netcap_dns_create_query(const char *name, libnet_t *l)
{
	uint8_t *payload = NULL;
	size_t pay_size = 0;

	uint32_t dns_server_ip = 0x08080808;
	uint32_t myip = libnet_get_ipaddr4(l);

	netcap_dns_create_query_record(name, &payload, &pay_size);

	libnet_build_dnsv4(
					LIBNET_UDP_DNSV4_H,		//header length
					0x1234,					//trans ID
					0x0100,					//flags
					0x0001,					//num of query
					0,						//num answers
					0,						//num authority
					0,						//additional record
					payload,
					pay_size,
					l,
					0);

	libnet_build_udp(
					0x3456,						//src port
					53,							//dst port
					LIBNET_UDP_H + LIBNET_UDP_DNSV4_H + pay_size,
					0,
					NULL,
					0,
					l,
					0);

	libnet_build_ipv4(
						LIBNET_IPV4_H + LIBNET_UDP_H + LIBNET_UDP_DNSV4_H + pay_size,
						0x00,
						0x00,
						0,
						0x80,
						IP_PROTO_UDP,
						0,
						myip,
						dns_server_ip,
						NULL,
						0,
						l,
						0);

	return 0;
}

int netcap_dns_create_query_record(const char *name, uint8_t **payload, size_t *pay_size)
{
	struct netcap_dns_question *ret = netcap_dns_question_new(name, 0x0001, 0x0001);

	*pay_size = ret->name_size + 4;
	*payload = (uint8_t*)malloc( *pay_size );
	if ( *payload )
	{
		memcpy(*payload, ret->name, ret->name_size);
		memcpy(*payload + ret->name_size, &(ret->type), sizeof(ret->type) );
		memcpy(*payload + ret->name_size + sizeof(ret->type), &(ret->class), sizeof(ret->class));
		free(ret);
		return 0;
	}
	return -1;
}

size_t netcap_dns_query_size(const char *name)
{
	return 4 + strlen(name) + 2;
}

struct netcap_dns_question* netcap_dns_question_new(const char *name, uint16_t type, uint8_t class)
{
	struct netcap_dns_question* ret;
	ret = (struct netcap_dns_question*)malloc( sizeof(*ret) );
	if ( ret )
	{
		netcap_dns_name_encode(name,  &(ret->name), &(ret->name_size) );
		ret->type = htons(type);
		ret->class = htons(class);
	}

	return ret;
}

int netcap_dns_rr_print(const uint8_t *packet)
{
	//we reuse query print function because the first 3 fields of query and rr are same
	netcap_dns_query_print(packet);

	packet += netcap_dns_query_len(packet);

	uint32_t *ttl = (uint32_t*)packet;
	printf("\nTTL: %02X", *ttl);;
	packet += 4;

	uint16_t *rdlen = (uint16_t*)packet;
	printf("\nRDLEN: %02X", *rdlen);

	std_util_hexdump(packet+2, *rdlen, "RDATA");

	return 0;
}

int netcap_dns_parse(uint8_t const *pkt, uint8_t const **query, uint8_t const **anws)
{
	struct libnet_dnsv4udp_hdr *hdr = (struct libnet_dnsv4udp_hdr*)pkt;
	*query = pkt + LIBNET_UDP_DNSV4_H;
	*anws = *query;

	//move pass question parts
	for ( uint16_t i = hdr->num_q; i > 0; )
	{
		uint8_t len = **anws;
		if ( len > 0 )
			*anws += len + 1;
		else
		{
			*anws += 5;
			--i;
		}
	}
	return 0;
}

int netcap_dns_query_print(const uint8_t *pkt)
{
	char *name;
	netcap_dns_name_decode(pkt, &name);
	printf("name: %s", name);

	pkt += netcap_dns_notation_len(pkt);
	printf("\ntype:  %02X%02X", *pkt, *(pkt+1));
	printf("\nclass: %02X%02X", *(pkt+2), *(pkt+3));

	free(name);
	return 0;
}

size_t netcap_dns_notation_len(const uint8_t *notation)
{
	size_t ret = 1;
	uint8_t len;

	while( (len = *notation) )
	{
		ret += len + 1;
		notation += len + 1;
	}

	return ret;
}

size_t netcap_dns_query_len(const uint8_t *query)
{
	return netcap_dns_notation_len(query) + 4;
}

size_t netcap_dns_rr_len(const uint8_t *rr)
{
	size_t ret = netcap_dns_query_len(rr) + 4;

	//move pointer to rdlen
	rr += ret;

	ret += 2 + ntohs(*((uint16_t*)rr));
	return ret;

}
