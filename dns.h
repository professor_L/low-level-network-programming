#ifndef		__PCAP_NET_DNS__
#define		__PCAP_NET_DNS__

#include <stdint.h>
#include <stdio.h>
#include <libnet.h>

#include "pcap_net_defines.h"
#include "std_utility.h"

/**/
struct netcap_dns_question {
	uint8_t *name;
	size_t name_size;
	uint16_t type;
	uint16_t class;
};

struct netcap_dns_rr {
	uint8_t *name;
	uint16_t type;
	uint16_t class;
	uint32_t ttl;
	uint16_t rdlen;
	uint8_t *rdata;
};


struct netcap_dns_payload {
	struct netcap_dns_question *question;
};

struct netcap_dns_question* netcap_dns_question_new(const char *name, uint16_t type, uint8_t class);
void netcap_dns_print(const uint8_t *pkt, FILE *fp);
int netcap_dns_name_encode(const char *name, uint8_t **notation, size_t *len);
int netcap_dns_name_decode(const uint8_t *notation, char **name);
int netcap_dns_create_query(const char *name, libnet_t *l);
int netcap_dns_create_query_record(const char *name, uint8_t **payload, size_t *pay_size);
int netcap_dns_rr_print(const uint8_t *packet);
int netcap_dns_parse(uint8_t const *pkt, uint8_t const **query, uint8_t const **anws);
int netcap_dns_query_print(const uint8_t *pkt);
size_t netcap_dns_notation_len(const uint8_t *notation);
size_t netcap_dns_query_len(const uint8_t *query);
size_t netcap_dns_rr_len(const uint8_t *rr);

#endif
