#ifndef		__STD_UTILITY__
#define		__STD_UTILITY__

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define STD_UTIL_MAX_STR_LEN	255

void std_util_hexdump(const uint8_t *data, const size_t size, const char *desc);
void std_util_fgets(char *s, int size, FILE *stream);
void std_util_random_bytes(uint8_t *arr, const size_t size);
uint8_t* std_util_get_random_bytes(const size_t size);

#endif
