#include "dhcp.h"
#include "std_utility.h"
#include "net_utility.h"

void netcap_dhcpv4_print_hdr(const struct libnet_dhcpv4_hdr *hdr)
{
	printf("\n------------ DHCPv4 header -------------------\n");
	netcap_dhcpv4_print_opcode(hdr->dhcp_opcode);
	netcap_dhcpv4_print_hwtype(hdr->dhcp_htype);
	printf("HW_addr len :  %hhu\n", hdr->dhcp_hlen);
	printf("Trans_ID    :  %u\n", ntohl(hdr->dhcp_xid));
	printf("Time eslaped:  %u\n", ntohs(hdr->dhcp_secs));
	net_util_print_ipv4(hdr->dhcp_cip,"client  IP");
	net_util_print_ipv4(hdr->dhcp_yip," your   IP");
	net_util_print_ipv4(hdr->dhcp_sip,"server  IP");
	net_util_print_ipv4(hdr->dhcp_gip,"gateway IP");
	
	std_util_hexdump(hdr->dhcp_chaddr, 16, "Client hardware address");
}

inline
void netcap_dhcpv4_print_opcode(const uint8_t opcode)
{
	switch ( opcode )
	{
		case LIBNET_DHCP_REQUEST:
			printf("opcode: REQUEST.\n");
			break;
		case LIBNET_DHCP_REPLY:
			printf("opcode: REPLY.\n");
			break;
		default:
			printf("Unknown opcode.\n");
	}
}

inline
void netcap_dhcpv4_print_hwtype(const uint8_t type)
{
	switch (type)
	{
		case DHCP_HWTYPE_ETHER:
			printf("HW_type: ETHERNET.\n");
			break;

		case DHCP_HWTYPE_IEEE802:
			printf("HW_type: IEEE802.\n");
			break;

		default:
			printf("Unknown HW type.\n");
	}
}

libnet_ptag_t netcap_dhcpv4_create_request_discovery(const uint32_t *xid, const uint8_t *mac, 
											libnet_t *l, const libnet_ptag_t *tag )
{
	//create options
	size_t pay_size;
	uint8_t *payload = netcap_dhcpv4_create_discovery_payload(&pay_size);

	//create DHCP packet
	libnet_ptag_t dhcp_tag = 0;
	dhcp_tag = libnet_build_dhcpv4(
						 LIBNET_DHCP_REQUEST,		//opcode
						 0x01,						//hardware type: ethernet
						 ETHER_ADDR_LEN,			//hardware addr len
						 0x00,						//hop count
						 *xid,						//trans ID
						 0x00, 						//second eslapsed
						 0x00, 						//boot flags
						 0x00,						//CIP
						 0x00,						//YIP
						 0x00,						//SIP
						 0x00,						//GIP
						 mac,						//client MAC address
						 NULL,						//server host name
						 NULL,						//boot file name
						 payload,					//payload = option
						 pay_size,					//payload size
						 l,							//libnet
						 *tag						//ptag
						 );

	libnet_ptag_t udp_tag = 0;
	udp_tag = libnet_build_udp(
						68,							//src port
						67,							//dst port
						LIBNET_UDP_H + LIBNET_DHCPV4_H + pay_size,
						0,
						NULL,
						0,
						l,
						udp_tag);

	//build IPv4 header
	uint32_t null_addr = 0x00;
	libnet_ptag_t ip_tag = 0;
	ip_tag = libnet_build_ipv4(
						LIBNET_IPV4_H + LIBNET_UDP_H + LIBNET_DHCPV4_H + pay_size,
						0x00,
						0x00,
						0,
						0x80,
						IP_PROTO_UDP,
						0,
						null_addr,
						0xFFFFFFFF,
						NULL,
						0,
						l,
						ip_tag);

	//build ethernet header
	libnet_ptag_t ether_tag = 0;
	uint8_t dst_mac[] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
	ether_tag = libnet_build_ethernet(
						dst_mac,
						mac,
						ETHERTYPE_IP,
						NULL,
						0,
						l,
						ether_tag);
	return dhcp_tag;
}

libnet_ptag_t netcap_dhcpv4_create_request_request(const uint32_t *xid, const uint8_t *mac, const uint32_t *req_ip,
											libnet_t *l, const libnet_ptag_t *tag)
{
	//create options
	size_t pay_size ;
	uint8_t *payload = netcap_dhcpv4_create_request_payload(&pay_size, req_ip);
	if ( payload == NULL )
		return -1;


	//create DHCP header
	libnet_ptag_t dhcp_tag = 0;
	dhcp_tag = libnet_build_dhcpv4(
						 LIBNET_DHCP_REQUEST,		//opcode
						 0x01,						//hardware type: ethernet
						 ETHER_ADDR_LEN,			//hardware addr len
						 0x00,						//hop count
						 *xid,						//trans ID
						 0x00, 						//second eslapsed
						 0x00, 						//boot flags
						 0x00,						//CIP
						 0x00,						//YIP
						 0x00,						//SIP
						 0x00,						//GIP
						 mac,						//client MAC address
						 NULL,						//server host name
						 NULL,						//boot file name
						 payload,					//payload = option
						 pay_size,					//payload size
						 l,							//libnet
						 *tag						//ptag
						 );

	//create UDP header
	libnet_ptag_t udp_tag = 0;
	udp_tag = libnet_build_udp(
						68,							//src port
						67,							//dst port
						LIBNET_UDP_H + LIBNET_DHCPV4_H + pay_size,
						0,
						NULL,
						0,
						l,
						udp_tag);
 	//create IP header
	uint32_t null_addr = 0x00;
	libnet_ptag_t ip_tag = 0;
	ip_tag = libnet_build_ipv4(
						LIBNET_IPV4_H + LIBNET_UDP_H + LIBNET_DHCPV4_H + pay_size,
						0x00,					//type of service
						0x00,					//ID
						0,						//fragment bits
						0x80,					//time to live
						IP_PROTO_UDP,			//upper proto
						0,						//check sum
						null_addr,				//src IP
						0xFFFFFFFF,				//dst IP
						NULL,
						0,
						l,
						ip_tag);

	//build ethernet header
	libnet_ptag_t ether_tag = 0;
	uint8_t dst_mac[] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
	ether_tag = libnet_build_ethernet(
						dst_mac,
						mac,
						ETHERTYPE_IP,
						NULL,
						0,
						l,
						ether_tag);
	free(payload);
	return dhcp_tag;
}

uint32_t netcap_dhcpv4_get_yip(const uint8_t *pkt)
{
	//point to start of DHCP
	pkt += LIBNET_ETH_H + LIBNET_IPV4_H + LIBNET_UDP_H;

	//point to YIP
	pkt += 16;

	uint32_t ret;
	memcpy(&ret, pkt, 4);
	return ntohl(ret);
}

uint8_t* netcap_dhcpv4_create_discovery_payload(size_t *pay_size)
{
	*pay_size = 28;
	uint8_t *ret = (uint8_t*)malloc( *pay_size );
	if ( ret == NULL )
		*pay_size = 0;
	else
	{
		//message type
		ret[0] = 0x35; ret[1] = 0x01; ret[2] = 0x01;

		//host name
		ret[3] = 0x0C; ret[4] = 0x07; memcpy(ret + 5, "someone", 7);

		//parameter request list
		ret[12] = 0x37; ret[13] = 0x0D;
		ret[14] = 0x01; ret[15] = 0x1C; ret[16] = 0x02; ret[17] = 0x03;
		ret[18] = 0x0F; ret[19] = 0x06; ret[20] = 0x77; ret[21] = 0x0C;
		ret[22] = 0x2C; ret[23] = 0x2F; ret[24] = 0x1A; ret[25] = 0x79;
		ret[26] = 0x2A;

		ret[27] = 0xFF;
	}

	return ret;
}

uint8_t* netcap_dhcpv4_create_request_payload(size_t *pay_size, const uint32_t *yip)
{
	uint8_t *temp = netcap_dhcpv4_create_discovery_payload(pay_size);
	uint8_t *ret = (uint8_t*)malloc( *pay_size + 6 );
	if ( ret == NULL )
		*pay_size = 0;
	else
	{
		memcpy(ret, temp, *pay_size);
		ret[2] = 0x03;	//change message type to "request"

		//add requested IP
		ret[*pay_size -1] = 0x32;
		ret[*pay_size] = 0x04;
		memcpy(ret + (*pay_size) + 1, yip, 4);

		//modify size and add END option
		*pay_size += 6;
		ret[*pay_size - 1] = 0xFF;
	}
	free(temp);
	return ret;
}
