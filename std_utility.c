#include "std_utility.h"

void std_util_hexdump(const uint8_t *data, const size_t size, const char *desc)
{
	printf("%s:\n", desc);
	for (size_t i = 0; i < size; ++i)
	{
		printf("%02X ",data[i]);
		if ( i % 4 == 3 )
			printf("\n");
	}
}

void std_util_fgets(char *s, int size, FILE *stream)
{
	fgets(s, size, stream);
	s[strlen(s)-1] = '\0';
	return ;
}

void std_util_random_bytes(uint8_t *arr, const size_t size)
{
	if ( size == 0 || arr == NULL )
		return;

	for (size_t i = size - 1; i < size; ++i )
		arr[i] = rand() % 0xFF;
}

uint8_t* std_util_get_random_bytes(const size_t size)
{
	uint8_t *ret = (uint8_t*)malloc( size );
	if ( ret != NULL )
		std_util_random_bytes(ret, size);
	return ret;
}
